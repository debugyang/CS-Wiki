# 👿 图

---

## 1. 基本的图算法

### ① 图的表示

两种图表示法：

- **邻接矩阵**（常用于稠密图）
- **邻接表**（常用）

<img src="https://gitee.com/veal98/images/raw/master/img/20201202205559.png" style="zoom: 50%;" />

<img src="https://gitee.com/veal98/images/raw/master/img/20201202205648.png" style="zoom:50%;" />

### ② 广度优先搜索 BFS（类似于层次遍历）

**基本思想** ：利用 `队列` 实现

- 首先访问起始顶点 V 并将其入队
- V 出队，并遍历 V 的所有邻接点 w1，w2，….，wn 并依次入队
- w1 出队，并遍历 w1 的全部邻接点(不包括已经被访问的点)
- w2 出队，并遍历 w2 的全部邻接点(不包括已经被访问的点)
- ....... 以此类推

广度优先遍历算法结果不唯一

<img src="https://gitee.com/veal98/images/raw/master/img/20201202211215.png" style="zoom:50%;" />

```java
// 二叉树数据结构
public class TreeNode {
	int data;
	TreeNode leftNode;
	TreeNode rightNode;
	public TreeNode() {
		
	}
	public TreeNode(int d) {
		data=d;
	}
	
	public TreeNode(TreeNode left,TreeNode right,int d) {
		leftNode=left;
		rightNode=right;
		data=d;
	}
}
```

广度优先遍历（伪代码）：

```java
// 广度优先遍历
void BFS(Graph G, int v){
    visit(v); //访问初始结点
    visited[v] = TRUE; //初始结点置已访问标识
    EnQueue(Q,v); //顶点v入队
    while(!isEmpty(Q)){
        DeQueue(Q,v); //顶点v出队
        for(w = FirstNeighbor(G,v); w>=0; w = NextNeighbor(G,v,w)){ //循环遍历v所有邻接点
            if(!visited[w]){
                visit(w); //访问顶点w
                visited[w] = TRUE;
                EnQueue(Q, w);
            }
        }
    } //while
}

void BFSTraverse(Graph G){
    for(i = 0; i<G.vexnum; i++)
        visited[i] = FALSE;
    InitQueue(Q); //初始化队列
    for(i = 0; i<G.vexnum;i++) //防止一次遍历无法遍历到全部结点（非连通图）
        if(!visited[i]) //若未被访问
            BFS(G,i);
}
```

### ③ 深度优先搜索 DFS（类似于先序遍历）

基本思想：利用 `递归/栈` 实现。当不能继续向下访问时，依次回退到最近的被访问结点

- 首先访问顶点 V，并将其标记为已访问
- 然后访问与顶点V的其中**一个**未被访问的邻接点W，并将其标记为已访问
- 再访问 W 的其中一个未被访问的邻接点，并将其标记为已访问
- 依次类推..... **当一个顶点所有的邻接顶点都被访问过时，则依次退回（回溯）最近被访问过的顶点**

深度优先遍历算法结果不唯一。

<img src="https://gitee.com/veal98/images/raw/master/img/20201202211615.png" style="zoom:50%;" />

利用**栈**来实现 DFS（伪代码）：

```java
// 深度优先遍历
void DFS(Graph G, int v){
    visit(v);
    visited[v] = TRUE;
    for (w = FirstNeighbor(G, v); w >= 0; w = NextNeighbor(G, v, w)){
        if(!visited[w])
            DFS(G,w); //递归
    }
}

void DFSTraverse(Graph G, int v){
    for(int i = 0 ; i<G.vexnum; i++)
        visited[v] = FALSE;
    for(int i = 0; i<G.vexnum; i++)
        if(!visited[v])
            DFS(G,i);
}
```

## 2. 最小生成树

设G = (V,E) 是无向连通带权图，即一个网络，E 中每条边 (v,w) 的权为 `c[v][w]`

**如果 G 的子图 G’ 是一棵<u>包含 G 的所有顶点</u>的树，则称 G’ 为 G 的<u>生成树</u>**。生成树上各边权的总和为该生成树的耗费

=> **在 G 的所有生成树中，<u>耗费最小</u>的生成树称为 G 的<u>最小生成树</u>**

> 💡 网络的最小生成树在实际中有广泛应用：
>
> 例如，在设计通信网络时，用图的顶点表示城市，用边 (v,w) 的权 `c[v][w]` 表示建立城市 v 和城市 w 之间的通信线路所需的费用。最小生成树就给出了建立通信网络的最经济的方案

**最小生成树的性质**：

<img src="https://gitee.com/veal98/images/raw/master/img/20201201221746.png" style="zoom:42%;" />

⭐用**贪心算法**可以设计出构造最小生成树的有效算法。Prim 算法和 Kruskal 算法用到了以上性质

### ① Prim 算法

设G=(V,E)是连通带权图，V={1,2,…,n}

Prim算法的基本思想：

- 首先置 S={1}
- 然后，只要 S 是 V 的真子集，就作如下的**贪心选择**：选取满足条件 i∈S，j∈V-S，且 `c[i][j]` 最小的边，将顶点 j 添加到 S 中
- 这个过程一直进行到 S=V 时为止
- 在这个过程中选取到的所有边恰好构成 G 的一棵最小生成树

举个例子：

<img src="https://gitee.com/veal98/images/raw/master/img/20201201222136.png" style="zoom:42%;" />

<img src="https://gitee.com/veal98/images/raw/master/img/20201201222156.png" style="zoom:55%;" />

在上述 Prim 算法中，还应当考虑如何有效地找出满足条件 i∈S，j∈V-S，且权  `c[i][j]` 最小的边 `(i,j)`，实现这个目的的较简单的办法是设置 2 个数组 `closest` 和 `lowcost``

- ``closest[j]` 是 j 在 S 中的邻接顶点，它与 j 在 S 中的其它邻接顶点 k 相比有 `c[j][closest[j]] ≤c[j][k]` 
- `lowcost[j]` 的值就是 `c[j][closest[j]]`

在Prim算法执行过程中，先找出 V-S 中使 `lowcost` 值最小的顶点 j，然后根据数组 `closest` 选取边 `(j,closest[j])`，最后将j添加到 S 中，并对 `closest` 和 `lowcost` 作必要的修改

用这个办法实现的 Prim 算法所需的时间复杂度为 $O(n^2)$

### ② Kruskal 算法

Kruskal 算法的基本思想：将图中的边按权值由小到大排序，由小到大顺序选取各条边，若选某边后不形成回路，则将其保留作为树的一条边

**Kruskal 算法贪心策略体现在每次都选取相对来说最小权值的边**

举个例子：

<img src="https://gitee.com/veal98/images/raw/master/img/20201201222839.png" style="zoom: 60%;" />

<img src="https://gitee.com/veal98/images/raw/master/img/20201201222858.png" style="zoom: 55%;" />

## 3. 单源最短路径

**单源最短路径问题**：

- 给定带权有向图 G =(V,E)，其中每条边的权是非负实数
- 给定 V 中的一个顶点，称为源
- 要求计算从源到所有其他各顶点的最短路长度（这里路的长度是指路上各边权之和）

### ① Dijkstra 算法

⭐ Dijkstra 算法其实是一种**贪心算法**。

基本思想：

- 设置顶点集合 S，初始时，S 中仅含有源，此后不断作贪心选择来扩充这个集合
- **一个顶点属于集合 S 当且仅当从源到该顶点的最短路径长度已知**
- 设 u 是 G 的某一个顶点，**把从源到 u 且中间只经过 S 中顶点的路称为从源到 u 的特殊路径**，并用数组 dist 记录当前每个顶点所对应的最短特殊路径长度
- **Dijkstra 算法每次从 V-S 中取出具有最短特殊路长度的顶点 u，将 u 添加到 S 中，同时对数组 dist 作必要的修改，检查 `dist(u)+[u,j]` 与 `dist[j]` 的大小，若 `dist(u)+[u,j]` 较小，则更新**
- 一旦 S 包含了所有 V 中顶点，dist 就记录了从源到所有其他顶点之间的最短路径长度

⭐ **Dijkstra 算法的贪心策略体现在总是在 V-S 中选择“最近”（具有最短特殊路长度）的顶点**

<img src="https://gitee.com/veal98/images/raw/master/img/20201201221314.png" style="zoom:50%;" />

### ② Bellman-Ford 算法

